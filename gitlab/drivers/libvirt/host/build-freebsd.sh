#!/bin/sh -x
# $1: project name
# $2: configure options
# $3: tar/gz package naem
# $4: ports build options to set
# $5: ports build options to unset

set -eo pipefail

# Prepare arguments:
# $1: project name
# $2: configure options
# $3: tar/gz package name
prepare() {
  (MAKE=gmake ./autogen.sh && \
    MAKE=gmake ./configure --disable-dependency-tracking $2 && \
    gmake dist-git)
  sudo mkdir -vp /usr/ports/distfiles "/usr/ports/security/$1"
  sudo cp -v $3-*.tar.gz /usr/ports/distfiles/
  LINKS="distinfo files Makefile pkg-descr pkg-plist"
  for link in $LINKS; do
    [ ! -e "./deploy/freebsd/$link" ] && continue
    [ -L "/usr/ports/security/$1/$link" ] && continue
    (sudo ln -vs "$(pwd)/deploy/freebsd/$link" /usr/ports/security/$1/)
  done
  depends="$(make -C "/usr/ports/security/$1" build-depends-list | cut -c 12-)"
  for pkg in $depends; do
    if ! sudo pkg install -y $pkg; then
      sudo make -C /usr/ports/$pkg install
    fi
  done
}

# Build arguments:
# $1: project name
# $2: options to set
# $3: options to unset
build() {
  options_name=$(make -C "/usr/ports/security/$1" -V OPTIONS_NAME)
  sudo make -C "/usr/ports/security/$1" BATCH=yes clean
  sudo make -C "/usr/ports/security/$1" BATCH=yes\
    ${options_name}_SET="$2" ${options_name}_UNSET="$3"\
    package
}

test -d "/usr/ports/security/$1" || prepare "$1" "$2" "$3"
build "$1" "$4" "$5"

exit 0
