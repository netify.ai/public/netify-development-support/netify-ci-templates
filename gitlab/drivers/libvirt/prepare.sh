#!/usr/bin/env bash

set -eo pipefail

BASE_PATH="$(realpath $(dirname "$0"))"
source "${BASE_PATH}/include.sh"

trap "exit ${SYSTEM_FAILURE_EXIT_CODE}" ERR

qemu-img create -f qcow2 -b "${VM_IMAGE_BASE}" "${VM_IMAGE}" -F qcow2

virt-install \
    --name "${VM_ID}" \
    --os-variant "${VM_VARIANT}" \
    --disk "${VM_IMAGE}" \
    --import \
    --vcpus=${VM_CPUS} \
    --ram=${VM_RAM_KB} \
    --network default \
    --graphics none \
    --noautoconsole

echo 'Waiting for a VM IP...'
for i in $(seq 1 ${VM_TIMEOUT_IP}); do
    VM_IP=$(get_vm_ip)

    if [ -n "${VM_IP}" ]; then
        echo "VM IP: ${VM_IP}"
        break
    fi

    if [ "${i}" == "${VM_TIMEOUT_IP}" ]; then
        echo "Failed to get VM IP in ${VM_TIMEOUT_IP} seconds..."
        exit "${SYSTEM_FAILURE_EXIT_CODE}"
    fi

    sleep 1s
done

echo 'Waiting for VM SSH access...'
for i in $(seq 1 ${VM_TIMEOUT_SSH}); do
    if ssh -i ${SSH_KEY} ${SSH_OPTIONS} ${SSH_USER}@${VM_IP} uname -a; then
        break
    fi

    if [ "${i}" == "${VM_TIMEOUT_SSH}" ]; then
        echo "Failed to get VM SSH access in ${VM_TIMEOUT_SSH} seconds..."
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi

    sleep 1s
done

echo 'Copying host files...'
scp -r -i ${SSH_KEY} ${SSH_OPTIONS} ~gitlab-runner/drivers/libvirt/host/* ${SSH_USER}@${VM_IP}:

echo "VM is ready."

exit 0
