#!/usr/bin/env bash

BASE_PATH="$(realpath $(dirname "$0"))"
source "${BASE_PATH}/include.sh"

echo "Destroying VM: ${VM_ID}..."
virsh destroy "${VM_ID}"

sleep 3s

echo "Removing (undefine) VM: ${VM_ID}..."
virsh undefine "${VM_ID}" --remove-all-storage

if [ -f "${VM_IMAGE}" ]; then
    echo "Deleting VM image: ${VM_IMAGE}"
    rm -vf "${VM_IMAGE}"
fi

exit 0
