#!/usr/bin/env bash

SSH_USER="gitlab-runner"
SSH_OPTIONS="-o StrictHostKeyChecking=no"
SSH_KEY="~gitlab-runner/.ssh/id_rsa.gitlab-runner"

VM_CPUS=8
VM_RAM_KB=2048
VM_TIMEOUT_IP=30
VM_TIMEOUT_SSH=30
VM_VARIANT="unknown"
VM_SHELL="/bin/sh"

VM_ID="runner-${CUSTOM_ENV_CI_RUNNER_ID}"
VM_ID+="-project-${CUSTOM_ENV_CI_PROJECT_ID}"
VM_ID+="-concurrent-${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}"
VM_ID+="-job-${CUSTOM_ENV_CI_JOB_ID}"

case "${CUSTOM_ENV_OS_NAME}" in
	freebsd)
		VM_VARIANT="freebsd13.1"
		VM_SHELL="/usr/local/bin/bash"
		;;
esac

VM_IMAGE_PATH="/home/gitlab-runner/images/"
VM_IMAGE_PATH+="${CUSTOM_ENV_OS_NAME}-${CUSTOM_ENV_OS_VERSION}"

VM_IMAGE_BASE="${VM_IMAGE_PATH}/base.qcow2"
VM_IMAGE="${VM_IMAGE_PATH}/${VM_ID}.qcow2"

get_vm_ip() {
	virsh -q domifaddr "${VM_ID}" |\
		awk '{print $4}' | sed -E 's|/([0-9]+)?$||'
}

