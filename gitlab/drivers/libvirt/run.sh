#!/usr/bin/env bash

set -eo pipefail

BASE_PATH="$(realpath $(dirname "$0"))"
source "${BASE_PATH}/include.sh"

VM_IP=$(get_vm_ip)

ssh -i ${SSH_KEY} ${SSH_OPTIONS} ${SSH_USER}@${VM_IP} ${VM_SHELL} < "${1}"

if [ $? -ne 0 ]; then
    exit "$BUILD_FAILURE_EXIT_CODE"
fi

exit 0
