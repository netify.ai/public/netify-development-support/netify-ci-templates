#!/bin/bash

set -eo pipefail

SKIP="${SKIP:-netify-proc-redis}"

ls -d netify-{proc,sink}-* | sort | while read DIR; do
    FULL_PATH="$(realpath "$DIR")"
    BASENAME="$(basename "$FULL_PATH")"

    IGNORE=0
    for PLUGIN in $SKIP; do
        if [ "$BASENAME" == "$PLUGIN" ]; then
            IGNORE=1
            break
        fi
    done

    [ $IGNORE -ne 0 ] && continue

    echo -e "[1m>>> ${BASENAME}:[0m Processing..."
    pushd "$FULL_PATH"

    eval $(echo $@ | sed "s/@plugin@/${BASENAME}/g");

    popd
    echo -e "[1m>>> ${BASENAME}:[0m Complete.\n"
done
