#!/bin/bash

if [ -z "$1" ]; then
  echo "$0 <tag message>"
  exit 1
fi

TAG="$1"

shift

tag_release() {
  pushd $2
  version=$(grep AC_INIT configure.ac | sed -e 's/^.*\[\([0-9\.]*\)\].*$/\1/')
  major=$(echo $version | cut -d. -f1)
  minor=$(echo $version | cut -d. -f2)
  point=$(echo $version | cut -d. -f3)
  echo -n "$2: v$major.$minor.$point -> "
  point=$[ $point + 1 ]
  if [ $point -gt 99 ]; then
    minor=$[ $minor + 1]
    point=0
  fi
  echo "v$major.$minor.$point"
  sed -i -e "s/^\(AC_INIT.*\[\)[0-9\.]*\(\].*\$\)/\1$major.$minor.$point\2/" configure.ac || exit $?
  git commit -a -m "$1 v$major.$minor.$point." || exit $?
  git push || exit $?
  git tag -a "v$major.$minor.$point" -m "$1 v$major.$minor.$point" || exit $?
  git push --tags
  popd
}

if [ $# -eq 0 ]; then
  ls -d netify-{proc,sink,plm}-* | sort | while read dir; do
    tag_release "$TAG" "$dir"
  done
else
  while [ $# -gt 0 ]; do
    tag_release "$TAG" "$1"
    shift
  done
fi

exit 0
