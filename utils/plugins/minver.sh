#!/bin/bash -x

MINVER=5.1.9
MINVER_PLM=1.0.28

find netify-{proc,sink}-* -name configure.ac |\
    xargs sed -i \
      -e "s/^NETIFY_MINVER=.*\$/NETIFY_MINVER=$MINVER/" \
      -e "s/^NETIFY_PLM_MINVER=.*\$/NETIFY_PLM_MINVER=$MINVER_PLM/"

find netify-{proc,sink}-* -name .gitlab-ci.yml |\
    xargs sed -i \
      -e "s/NETIFY_MINVER: .*\$/NETIFY_MINVER: \"$MINVER\"/" \
      -e "s/NETIFY_PLM_MINVER: .*\$/NETIFY_PLM_MINVER: \"$MINVER_PLM\"/"
