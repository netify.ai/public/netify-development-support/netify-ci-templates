#!/bin/sh

export MAKEOPTS=-j5

#BRANCH="master"
BRANCH="v5.0.x"

AGENT="git@gitlab.com:netify.ai/public/netify-agent.git"
PLM="git@gitlab.com:netify.ai/private/netify-plm.git"
PLUGINS="git@gitlab.com:netify.ai/private/netify-plugins/netify-proc-aggregator.git git@gitlab.com:netify.ai/private/netify-plugins/netify-proc-dev-discovery.git git@gitlab.com:netify.ai/private/netify-plugins/netify-proc-flow-actions.git git@gitlab.com:netify.ai/private/netify-plugins/netify-sink-log.git git@gitlab.com:netify.ai/private/netify-plugins/netify-sink-mqtt.git git@gitlab.com:netify.ai/public/netify-plugins/netify-proc-core.git git@gitlab.com:netify.ai/public/netify-plugins/netify-sink-http.git git@gitlab.com:netify.ai/public/netify-plugins/netify-sink-socket.git"

if [ ! -d /usr/ports/distfiles ]; then
    rm -vf /usr/ports/distfiles
    mkdir -vp /usr/ports/distfiles
fi

name() {
	echo $(basename "$1" | sed -e 's/\.git$//g')
}

clone() {
	project=$(name "$1")
	if [ -d "$project" ]; then
		(cd "$project" && git pull) || exit $?
		(cd "$project" && git checkout ${BRANCH}) || exit $?
		(cd "$project" && git pull) || exit $?
		(cd "$project" && git submodule update) || exit $?
	else
		git clone --recursive "$1" || exit $?
		(cd "$project" && git checkout ${BRANCH}) || exit $?
		(cd "$project" && git submodule update) || exit $?
	fi
}

prepare() {
	project=$(name "$1")
	if [ ! -z "$3" ]; then
		package="$3"
	else
		package="$project"
	fi

	(cd "$project" && ./autogen.sh && MAKE=gmake ./configure "$2" && gmake dist-git) || exit $?
	sudo mkdir -vp "/usr/ports/security/$project" || exit $?
	(cd "$project" && sudo cp -v $package-*.tar.gz /usr/ports/distfiles) || exit $?

	LINKS="distinfo files Makefile pkg-descr pkg-plist"
	for link in $LINKS; do
		[ ! -f "$HOME/$project/deploy/freebsd/$link" ] && continue
		[ -L "/usr/ports/security/$project/$link" ] && continue
		(cd "/usr/ports/security/$project" && sudo ln -vs "$HOME/$project/deploy/freebsd/$link" .) || exit $?
	done

	if [ -d "$HOME/$project/deploy/freebsd/files" ]; then
		sudo rm -rf "/usr/ports/security/$project/files"
		sudo cp -a "$HOME/$project/deploy/freebsd/files" "/usr/ports/security/$project/"
	fi

	DEPENDS=$(cd "/usr/ports/security/$project" && sudo make build-depends-list | cut -c 12-)
	for pkg in $DEPENDS; do
		echo ">>> Installing $pkg (from pkg)..."
		if ! sudo pkg install -y $pkg; then
			echo ">>> Installing $pkg (from ports)..."
			sudo make -C /usr/ports/$pkg || exit $?
			sudo make FORCE_PKG_REGISTER=1 -C /usr/ports/$pkg install || true
		fi
	done
}

build() {
	project=$(name "$1")
	options_name=$(sudo make -C "/usr/ports/security/$project" -V OPTIONS_NAME)
	(cd "/usr/ports/security/$project" && sudo make BATCH=yes clean) || exit $?
	(cd "/usr/ports/security/$project" && sudo make ${options_name}_SET="$2" ${options_name}_UNSET="$3" BATCH=yes package) || exit $?
}

install() {
	project=$(name "$1")
	(cd "/usr/ports/security/$project" && sudo make BATCH=yes reinstall) || exit $?
}

TOOLS="autoconf automake git gmake gtar libtool pkgconf sudo"

check_tools() {
	for tool in $TOOLS; do
		if ! which "$tool"; then
			echo "${tool}: not found"
			exit 1
		fi
	done
}

check_tools

clone "$AGENT"
prepare "$AGENT" "--disable-libtcmalloc" netifyd
build "$AGENT" "DEVEL"
install "$AGENT"
build "$AGENT" "" "DEVEL"

clone "$PLM"
prepare "$PLM"
build "$PLM" "DEVEL"
install "$PLM"
build "$PLM" "" "DEVEL"

for plugin in $PLUGINS; do
	project=$(name "$1")
	case "$project" in
	*)
		clone "$plugin"
		prepare "$plugin"
		build "$plugin"
	;;
	esac
done

exit $?
